data class Member(
        private val id: Int = 0,
        private val firstName: String = "",
        private val lastName: String = "",
        private val year: Year = Year.Unknown) {
    // Intentionally blank
}

enum class Year(val year: Int) {
    Unknown(0),
    Freshman(1),
    Sophomore(2),
    Junior(3),
    Senior(4),
    Grad(5);
}