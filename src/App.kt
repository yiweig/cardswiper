class App() {

    private val cardReader = CardReader()
    private val recordWriter = RecordWriter()

    fun run() {
        while (true) {
            val input = cardReader.prompt()
            if (input.isNullOrBlank()) {
                println("Null/blank input found!")
            } else if (input!!.toLowerCase().equals("exit")) {
                println("Exiting...\n")
                break
            } else {
                recordWriter.writeLine(
                        Member(validate(input).toInt()), input)
            }
        }
        recordWriter.close()
    }

    private fun validate(input: String): String {
        val result = input.replace(Regex("[^\\d]+"), "")
        if (result.isBlank()) {
            return "0"
        } else if (result.length > 7) {
            return result.substring(0, 7)
        } else {
            return result
        }
    }
}