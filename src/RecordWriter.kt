import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

class RecordWriter() {

    private val fileName = "members.csv"
    private var writer: BufferedWriter

    init {
        if (File(fileName).isFile) {
            writer = BufferedWriter(FileWriter(fileName, true))
        } else {
            writer = BufferedWriter(FileWriter(fileName, false))
            writeLine("member | original input")
        }
    }

    private fun writeLine(line: String) {
        writer.write(line)
        writer.newLine()
    }

    fun writeLine(member: Member, originalInput: String) {
        writeLine("$member | $originalInput")
    }

    fun close() {
        writer.close()
    }
}